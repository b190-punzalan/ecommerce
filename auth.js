const jwt=require("jsonwebtoken");

// used in algorithm for encrypting our data which makes it difficult to decode the info without the defined secret keyword (for more security purposes ito. halimbawa, nadecode ni hacker yung information from the user pero hindi niya alam yung secret code mo)
const secret="BakitMasarapKumain"

module.exports.createAccessToken=(user)=>{
	const data={
		id:user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
}


// Token Verification
// Verify Method
module.exports.verify=(req, res, next)=>{
	// the token is retrieved from the request header
	// POSTMAN - Authorization-Bearer Token
	let token=req.headers.authorization;
	if (typeof token !== "undefined"){
		console.log(token);
		// the token sent is a type of "Bearer" token which when received, contains the "Bearer " as a prefix to the string
		// yung Bearer hindi natin kailangan but only the token itself so si slice inaalis niya yung bearer at space para yung mismong token lang ang irereturn niya
		token=token.slice(7, token.length)
		// verify()-validates the token decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data)=>{
			// the jwt is invalid pag nag-error
			if (err){
				return res.send({auth:"failed"});
			}else{
				// si next() allows the application to proceed with the next middleware function/callback function in the route
				next();
			}
		})
	}else{
		return res.send({auth:"failed"});
	}
};

// Token Decryption
module.exports.decode=(token)=>{
	if (typeof token !== "undefined"){
		token=token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) =>{
			if (err){
				return null;
			}else{
				// "decode" method is used to obtain the info from the JWT
				// the "token" argument serves a the one to be decoded
				// {complete: true} - option that allows us to return add'l info from the JWT
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}else{
		// if the token does not exist
		return null;
	};
};