const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String, 
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String, 
		required: [true, "Last Name is required"]
	},
	email: {
		type: String, 
		required: [true, "email is required"]
	},
	password:{
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNumber:{
		type: String,
		required: [true, "Mobile number is required"]
	}, 
	orderPlaced:[
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				default: 1
			},
			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);