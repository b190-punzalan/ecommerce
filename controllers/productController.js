const Product=require("../models/products.js");
// const Order=require("../models/orders.js");

// create product (admin)
// module.exports.addProduct = ( reqBody ) => {
// 	let newProduct = new Product({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price,
// 		size: reqBody.size
// 	});
// 	return newProduct.save().then((product, error) => {
// 		if (error){
// 			return false;
// 		} else{
// 			return true;
// 		}
// 	})
// }


// create product (admin) - use below code as it displays details instead of true or false
module.exports.addProduct = (userData, productData) => 
		{
		    if (userData.isAdmin) 
		    {
		        let newProduct = new Product({
		            name: productData.name,
		            description: productData.description,
		            price: productData.price,
		            size: productData.size
		        });
		        return newProduct.save().then((product, error) =>
		        {
		            if (error) 
		            {
		                console.log(error)
		                return false;
		            } 
		            else 
		            {
		                return product;
		            }
		        })
		    } 
		    else 
		    {
		        return Promise.reject('Unauthorised user');
		    }
		};

// retrieve all active products
module.exports.getActiveProducts=(req,res)=>{
	if({isActive: true}){
		return Product.find({isActive: true}).then(result=>{return result;
	});
	}else{
		return false;
	}
};

// get all products
module.exports.getAllProducts=()=>{
	return Product.find({}).then(result=>{return result
	})
};

// retrieving a specific product by ID
module.exports.getProduct=(reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>{return result;
	})
};


// updating a product
module.exports.updateProduct=(reqParams, reqBody)=>{
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		size: reqBody.size
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, err)=>{
		if (err){
			return false;
		} else{
			return true; 
		}
	})
};

// archive a product
module.exports.archiveProduct = (reqParams, reqBody) => {
	let updateActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// checkout 
module.exports.checkout=async(data)=>{
	// adding the productId in the orderPlaced array of the user
	let isUserUpdated=await User.findById(data.userId).then(user=>{
		user.orderPlaced.push({productId: data.productId, quantity: data.quantity, totalAmount: data.totalAmount});
		// saves the updated user info in the DB
		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	let isProductUpdated=await Product.findById(data.productId).then(product=>{
		if({isActive: false}){
			return false;
		}else{
		product.orderReceived.push({userId: data.userId, quantity: data.quantity, totalAmount: data.totalAmount});
		return product.save().then((product, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}
});
	// condition that will check if the user and product documents have been updated:
	if(isUserUpdated && isProductUpdated){
		return true;
	}else{
		// User checkout failed:
		return false;
	}
}


// retrieve all orders received - admin only

// module.exports.getAllOrders=()=>{
// 	return Product.find({}).then(result=>{return result
// 	})
// };