const User = require("../models/users.js");
const Product = require("../models/products.js");
// const Order = require("../models/orders.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");

// registration
module.exports.registerUser=(reqBody)=>{
	let newUser= new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNumber: reqBody.mobileNumber,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// authentication
module.exports.loginUser=(reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result =>{
		// if the user email does not exist:
		if (result===null){
			return false;
		// if the user email exists in the DB:
		}else{
			const isPasswordCorrect=bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access:auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}
// check if email exists
// module.exports.checkEmailExists=(reqBody)=>{
// 	return User.find({email: reqBody.email}).then(result=>{
// 		if(result.length>0){
// 			return true;
// 			// meron existing email kaya true
// 		}else{
// 			return false;
// 		}
// 	})
// }

// get all users
// module.exports.getAllUsers=()=>{
// 	return User.find({}).then(result=>{return result
// 	})
// };

// set user as admin
module.exports.setAsAdmin = (reqParams, reqBody) => {
	let updateAdminField = {
		isAdmin : true
	};
	return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};



// checkout 
module.exports.checkout=async(data)=>{
	// adding the productId in the orderPlaced array of the user
	let isUserUpdated=await User.findById(data.userId).then(user=>{
		user.orderPlaced.push({productId: data.productId, quantity: data.quantity, totalAmount: data.totalAmount});
		// saves the updated user info in the DB
		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	let isProductUpdated=await Product.findById(data.productId).then(product=>{
		if({isActive: false}){
			return false;
		}else{
		product.orderReceived.push({userId: data.userId, quantity: data.quantity, totalAmount: data.totalAmount});
		return product.save().then((product, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}
});
	if(isUserUpdated && isProductUpdated){
		return true;
	}else{
		// User checkout failed:
		return false;
	}
}

// // get own orders of users
module.exports.getMyOrders=(reqBody)=>{
	return User.find({orderPlaced: reqBody.orderPlaced}).then(result=>{return result
	});
};


// get all orders of users
module.exports.getAllOrders=()=>{
	return User.find({}).then(result=>{return result
	});
};

