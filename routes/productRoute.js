const express = require("express");
const router=express.Router();
const productController=require("../controllers/productController.js");
const auth = require("../auth.js");


// Route for creating a product 
// router.post("/", auth.verify, (req, res) => {
// 	if(auth.decode(req.headers.authorization).isAdmin === false) {
// 		res.send(false);
// 	} else {
// 		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
// 	}
// });


// Also a Route for creating a product 
router.post('/', auth.verify, (req, res) => {
		const userData = auth.decode(req.headers.authorization);
	    productController.addProduct({isAdmin: userData.isAdmin}, req.body).then(result => res.send(result)).catch(error => res.send(error));
});



// route for retrieving all products
router.get('/all', (req, res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});

// route for retrieving active products
router.get('/active', (req, res)=>{
	productController.getActiveProducts().then(resultFromController=>res.send(resultFromController));
});

// route for retrieving a single product
router.get('/:productId', (req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController))
});

// updating a product
router.put('/:productId', auth.verify, (req, res)=>{
	productController.updateProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController));
});


// archive a product
router.put("/:productId/archive", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin===false){res.send(false);
	}else{
	productController.archiveProduct(req.params).then(resultFromController=>res.send(resultFromController));
	}
});

// route for Non-admin create order 
router.post("/checkout", auth.verify, (req, res)=>{
		let data={
			userId: auth.decode(req.headers.authorization).id,
			productId: req.body.productId,
			quantity: req.body.quantity,
			totalAmount: req.body.totalAmount
		}
		productController.checkout(data).then(resultFromController=>res.send(resultFromController));
});

// retrieve orders- admin only 
// router.get('/checkout', (req, res)=>{
// 	if(auth.decode(req.headers.authorization).isAdmin===false){res.send(false);
// 	}else{
// 	productController.getAllOrders().then(resultFromController=>res.send(resultFromController));
// 	}
// });


module.exports= router;