const express = require("express");
const router=express.Router();
const userController=require("../controllers/userController.js");
const auth=require("../auth.js");

// route for user registration
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

// route for user authentication
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});

// route for check email
// router.post('/checkEmail', (req,res)=>{
// 	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
// })

// // route to get all users
// router.get('/all', (req, res)=>{
// 	userController.getAllUsers().then(resultFromController=>res.send(resultFromController));
// });

// route to set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin===false){res.send(false);
	}else{
	userController.setAsAdmin(req.params).then(resultFromController=>res.send(resultFromController));
	}
});

// route for Non-admin create order - kung hindi ihihiwalay si order schema
router.post("/checkout", auth.verify, (req, res)=>{
		let data={
			userId: auth.decode(req.headers.authorization).id,
			productId: req.body.productId,
			quantity: req.body.quantity,
			totalAmount: req.body.totalAmount
		}
		userController.checkout(data).then(resultFromController=>res.send(resultFromController));
});

// route to get order of a user
router.get('/myOrders', auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin===true){res.send(false);
	}else{
	userController.getMyOrders(reqBody).then(resultFromController=>res.send(resultFromController));
	}	
});

// route to get all orders of users
router.get('/checkout', (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin===false){res.send(false);
	}else{
	userController.getAllOrders().then(resultFromController=>res.send(resultFromController));
	}
});

module.exports=router;