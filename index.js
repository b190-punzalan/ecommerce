const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// routes
const userRoute=require("./routes/userRoute.js");
const productRoute=require("./routes/productRoute.js");


const app = express();

mongoose.connect("mongodb+srv://jpunzalan:iloveNZ@wdc028-course-booking.0tib1.mongodb.net/Delish-Cheesecake?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=>console.log("we're connected to the database"));

// allows all resources to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);
app.use("/products", productRoute);


app.listen(process.env.PORT||4000, ()=>{console.log(`API now online at port ${process.env.PORT||4000}`)});